﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GT.Application.Filters
{
    public class SessionTimeoutActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden,"La sessión del usuario ha caducado");
        }
    }
}