﻿using System.Web;
using System.Web.Optimization;

namespace GT.Application
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Vendor CSS
            bundles.Add(new StyleBundle("~/Content/bootstrap/css").Include(
                      "~/Content/vendor/bootstrap/css/bootstrap.css",
                      "~/Content/vendor/font-awesome/css/font-awesome.css",
                      "~/Content/vendor/magnific-popup/magnific-popup.css"));
            //Theme CSS
            bundles.Add(new StyleBundle("~/Content/theme/css").Include(
                      "~/Content/css/theme.css",
                      "~/Content/css/skins/default.css",
                      "~/Content/css/theme-custom.css"));

            //plugins CSS
            bundles.Add(new StyleBundle("~/Content/plugins/css").Include(
                      "~/Content/vendor/select2/select2.css",
                      "~/Content/vendor/jquery-datatables-bs3/assets/css/datatables.css",
                      "~/Content/vendor/pnotify/pnotify.custom.css"
                      ));
            //Forms
            bundles.Add(new StyleBundle("~/Content/forms").Include(
                            "~/Content/vendor/bootstrap-multiselect/bootstrap-multiselect.css",
                            "~/Content/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css",
                            "~/Content/vendor/dropzone/css/dropzone.css"
                            )); 
            //CodeMirror
            bundles.Add(new StyleBundle("~/Content/codemirror").Include(
                            "~/Content/vendor/codemirror/lib/codemirror.css",
                            "~/Content/vendor/codemirror/theme/monokai.css"
                            ));

               
                  //==============================================================

                  //Head Libs

                  bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/vendor/modernizr/modernizr.js"));


            //Vendors
            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                         "~/Content/vendor/jquery/jquery.js",
                          "~/Content/vendor/jquery-browser-mobile/jquery.browser.mobile.js",
                        "~/Content/vendor/bootstrap/js/bootstrap.js",
                        "~/Content/vendor/nanoscroller/nanoscroller.js",
                        "~/Content/vendor/magnific-popup/magnific-popup.js",
                        "~/Content/vendor/jquery-placeholder/jquery.placeholder.js"
                        ));

            //Plugins JS
            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                     "~/Content/vendor/select2/select2.js",
                     "~/Content/vendor/jquery-datatables/media/js/jquery.dataTables.js",
                     "~/Content/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js",
                     "~/Content/vendor/jquery-datatables-bs3/assets/js/datatables.js",
                     "~/Content/vendor/pnotify/pnotify.custom.js"
                     ));

            //Theme JS
            bundles.Add(new ScriptBundle("~/bundles/theme").Include(
                     "~/Content/js/theme.js",
                     "~/Content/js/theme.custom.js",
                     "~/Content/js/theme.init.js"
                     ));

            //Validation JS
            bundles.Add(new ScriptBundle("~/bundles/validation").Include(
                     "~/Content/vendor/jquery-validation/jquery.validate.js",
                     "~/Content/js/forms/examples.validation.js"
                     ));

            //Forms
            bundles.Add(new ScriptBundle("~/bundles/forms").Include(
                               "~/Content/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js",
                               "~/Content/vendor/dropzone/dropzone.js",
                               "~/Content/vendor/jquery-maskedinput/jquery.maskedinput.js"
                               ));
            //CodeMirror
            bundles.Add(new ScriptBundle("~/bundles/codemirror").Include(
                               "~/Content/vendor/codemirror/lib/codemirror.js",
                               "~/Content/vendor/codemirror/addon/selection/active-line.js",
                               "~/Content/vendor/codemirror/addon/edit/matchbrackets.js",
                               "~/Content/vendor/codemirror/mode/javascript/javascript.js",
                               "~/Content/vendor/codemirror/codemirror/mode/xml/xml.js",
                               "~/Content/vendor/codemirror/mode/htmlmixed/htmlmixed.js",
                               "~/Content/vendor/codemirror/mode/css/css.js"
                               ));

            //Vue JS
            bundles.Add(new ScriptBundle("~/bundles/vue").Include(
   "~/Scripts/vue/vue.js",
   "~/Scripts/vue/vue-resource.js"));

        }
    }
}
