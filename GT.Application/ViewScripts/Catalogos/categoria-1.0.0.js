﻿$(function () {
    var categoria = new Vue({
        el: '#container',
        data:
        {
            urls: {
                listar: baseUrl("Categoria/Listar"),
                registrar: baseUrl("Categoria/Registrar"),
                actualizar: baseUrl("Categoria/Editar"),
                getinfo: baseUrl("Categoria/ObtenerInfo"),
                baja: baseUrl("Categoria/Baja")
            },
            errorRegistroCategoria: {
                msg: "",
                css: "alert-default"
            },
            isLoading: false,
            isEdit: false,
            modelCategoria: {
                FiId: 0,
                FcNombre: "",
                FcDescripcion: "",
                FlActivo: true
            },
            listCategorias: []
        },
        created: function () {
            this.listarCategorias();
        },
        computed: {
            formCategoriaCompleto: function () {
                return (
                     this.modelCategoria.FcNombre !== ""
                    && this.modelCategoria.FcDescripcion !== ""
                );
            }
        },
        watch: {

        },
        methods: {
            seleccion: function (e) {
                console.log(e);
            },
            limpiarFormRegistro: function () {
                this.errorRegistroCategoria = {
                    msg: "",
                    css: "alert-default"
                };
                this.modelCategoria.FiId = 0;
                this.modelCategoria.FcNombre = "";
                this.modelCategoria.FcDescripcion = "";
                this.modelCategoria.FlActivo = true;
            },
            closeModal: function () {
                $(".modal").modal("hide");
            },
            listarCategorias: function () {
                this.isLoading = true;

                this.$http.post(this.urls.listar, { params: { todos: true } }).then(function (response) {
                    var resp = response.body.model;
                    if (resp !== 'undefined' && resp !== null) {
                        this.listCategorias = resp.result;
                        this.initDataTable();
                    }
                    else {
                        this.errorRegistroCategoria.msg = "No se encontro información disponible. Intente mas tarde";
                        this.errorRegistroCategoria.css = "alert-info";
                    }

                    this.isLoading = false;
                }, function (err) {
                    this.errorRegistroCategoria.msg = err.status + ' : ' + err.statusText;
                    this.errorRegistroCategoria.css = "alert-danger";
                    this.isLoading = false;
                });
            },
            obtenerCategoria: function (id) {
                this.isLoading = true;

                this.$http.get(this.urls.getinfo, { params: { id: id } }).then(function (response) {
                    var resp = response.body.model;
                    if (resp !== 'undefined' && resp !== null) {
                        this.modelCategoria.FiId = resp.FiId;
                        this.modelCategoria.FcNombre = resp.FcNombre;
                        this.modelCategoria.FcDescripcion = resp.FcDescripcion;
                        this.modelCategoria.FlActivo = resp.FlActivo;
                    }
                    else {
                        this.errorRegistroCategoria.msg = "No se encontro información disponible. Intente mas tarde";
                        this.errorRegistroCategoria.css = "alert-info";
                    }
                    this.isLoading = false;
                }, function (err) {
                    this.errorRegistroCategoria.msg = err.status + ' : ' + err.statusText;
                    this.errorRegistroCategoria.css = "alert-danger";
                    this.isLoading = false;
                });
            },
            registrarEditarCategoria: function () {
                this.isLoading = true;
                this.$http.post(this.isEdit ? this.urls.actualizar : this.urls.registrar, this.modelCategoria)
                    .then(function (response) {
                        var resp = response.body.model;
                        if (!resp.success) {
                            if (resp.mensaje !== null && resp.mensaje.length > 0) {
                                this.errorRegistroCategoria.msg = resp.mensaje;
                                this.errorRegistroCategoria.css = "alert-info";
                                this.isLoading = false;
                            }
                        }
                        else {
                            if (resp.alerta !== null && resp.alerta.length > 0) {
                                //$.notify(resp.alerta, "success");
                                new PNotify({
                                    title: 'Correcto',
                                    text: resp.alerta,
                                    type: 'success'
                                });

                                this.listarCategorias();
                            }
                            this.limpiarFormRegistro();
                            this.isLoading = false;
                            this.closeModal();
                        }
                    }, function (err) {
                        this.errorRegistroCategoria.msg = err.status + ' : ' + err.statusText;
                        this.errorRegistroCategoria.css = "alert-danger";
                        this.isLoading = false;
                    });
            },
            bajaCategoria: function (id) {
                if (confirm('¿Esta seguro de INACTIVAR la categoria?')) {
                    this.isLoading = true;

                    this.$http.post(this.urls.baja, { id: id }).then(function (response) {
                        var resp = response.body.model;
                        if (resp.success === false) {
                            if (resp.mensaje !== null && resp.mensaje.length > 0) {
                                this.errorRegistroCategoria.msg = resp.mensaje;
                                this.errorRegistroCategoria.css = "alert-warning";
                                //$.notify(resp.mensaje, "error");
                                new PNotify({
                                    title: 'Error',
                                    text: resp.mensaje,
                                    type: 'error'
                                });
                                this.isLoading = false;
                            }
                        }
                        else {
                            if (resp.alerta !== null && resp.alerta.length > 0) {
                                new PNotify({
                                    title: 'Correcto',
                                    text: resp.alerta,
                                    type: 'success'
                                });
                                this.listarCategorias();
                            }
                            this.limpiarFormRegistro();
                            
                            this.isLoading = false;
                            
                        }
                    }, function (err) {
                        this.errorRegistroCategoria.msg = err.status + ' : ' + err.statusText;
                        this.errorRegistroCategoria.css = "alert-danger";
                        this.isLoading = false;
                    });
                }
            },
            openEdit: function (id) {
                this.limpiarFormRegistro();
                this.obtenerCategoria(id);
                this.isEdit = true;
            },
            initDataTable: function () {
                setTimeout(function (self) {
                    $('#datatable').dataTable();
                }, 1000, this);

            }
        }
    });
});