﻿using GT.Application.ServiceCatalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace GT.Application.Models.Catalogos
{

    public class CategoriaModel
    {
        private string bindigDifusion = "BasicHttpBinding_IServiceCatalogos";


        public VMCat_Categoria RegistrarCategoria(VMCat_Categoria modelo)
        {
            try
            {
                ServiceCatalogosClient cliente = new ServiceCatalogosClient(bindigDifusion);
                VMCat_Categoria resp = cliente.RegistrarCategoria(modelo);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public VMCat_Categoria ObtenerCategoria(short id)
        {
            try
            {
                ServiceCatalogosClient cliente = new ServiceCatalogosClient(bindigDifusion);

                VMCat_Categoria resp = cliente.ObtenerInfoCategoria(id);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void EditarCategoria(VMCat_Categoria modelo)
        {
            try
            {
                ServiceCatalogosClient cliente = new ServiceCatalogosClient(bindigDifusion);

                cliente.EditarCategoria(modelo);
                cliente.Close();
            }
            catch (FaultException<ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public VMCat_Categoria[] ListarCategorias(bool todos)
        {
            try
            {
                ServiceCatalogosClient cliente = new ServiceCatalogosClient(bindigDifusion);
                VMCat_Categoria[] resp = cliente.ListarCategorias(todos);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void BajaCategoria(short id)
        {
            try
            {
                ServiceCatalogosClient cliente = new ServiceCatalogosClient(bindigDifusion);
                cliente.BajaCategoria(id);
                cliente.Close();
            }
            catch (FaultException<ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }
    }
}