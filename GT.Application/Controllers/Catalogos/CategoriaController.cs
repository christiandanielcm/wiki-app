﻿using GT.Application.Filters;
using GT.Application.Models.Catalogos;
using GT.Application.ServiceCatalogos;
using GT.Application.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GT.Application.Controllers.Catalogos
{
    public class CategoriaController : Controller
    {
        CategoriaModel moduloModel = new CategoriaModel();

        // GET: Categoria
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public JsonResult Registrar(VMCat_Categoria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                resp.result = moduloModel.RegistrarCategoria(model);
                resp.success = true;
                resp.alerta = "Categoria registrada correctamente";
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerInfo(short id)
        {
            VMCat_Categoria model = moduloModel.ObtenerCategoria(id);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Editar(VMCat_Categoria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {

                moduloModel.EditarCategoria(model);
                resp.result = model;
                resp.success = true;
                resp.alerta = "Información actualizada correctamente";

            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }


            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Baja(short id)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {

                moduloModel.BajaCategoria(id);
                resp.success = true;
                resp.alerta = "Información actualizada correctamente";

            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }


            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [SessionTimeoutActionFilter]
        [HttpPost]
        public JsonResult Listar(bool todos = true)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                resp.result = moduloModel.ListarCategorias(todos);
                resp.success = true;
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

    }
}
