﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GT.Application.VM
{
    public class RespuestaModel
    {
        public bool success { get; set; }
        public List<object> results { get; set; }
        public object result { get; set; }
        public string mensaje { get; set; }
        public string funcion { get; set; }
        public string href { get; set; }
        public string alerta { get; set; }
        public string target { get; set; }
        public string html { get; set; }

        /// <summary>
        /// constructor
        /// </summary>
        public RespuestaModel()
        {
            this.success = false;
            this.mensaje = string.Empty;
        }

        public void SetRespuesta(bool respuesta, string msj = null)
        {
            if (!respuesta && msj == "")
            {
                mensaje = "Ocurrio un error inesperado";
            }
            else
            {
                this.mensaje = msj;
            }
            this.success = respuesta;
        }
    }
}